import java.util.*;

public class StartGame {
    private final ArrayList<String> gamersNames = new ArrayList<>();

    public void start() {

        // Hello massage
        System.out.println("Hello! Dice game welcomes you!");
        System.out.println("-----------------");

        // Enter the number of the players
        String input = "";
        int gamersNumber = 0;
        boolean inputValid = false;

        Scanner scanner = new Scanner(System.in);

        // Write exceptions
        while (!inputValid) {
            try {
                System.out.println("Enter the number of players: ");
                input = scanner.nextLine();
                gamersNumber = Integer.parseInt(input);
                inputValid = true;
            } catch (NumberFormatException e) {
                System.out.println("You entered " + input + ". " + "Please enter the NUMBER of players!");
            }

        }

        // Read the names
        for (int i = 1; i <= gamersNumber; i++) {
            System.out.println("Enter the name of the " + i + "-th" + " player:");
            String firstName = scanner.nextLine();
            this.gamersNames.add(firstName);
        }

        dice();

    }

    // Game process
    public void dice() {

        Random random = new Random();
        int maxScore = 0;
        final int MAXIMUM_COMBINATION = 6;
        final int MINIMUM_COMBINATION = 1;
        String champion = "";

        HashMap<String, Integer> champions = new HashMap<>();

        for (String gamer : gamersNames) {
            System.out.println("-----------------");
            System.out.println("The throw is made by the player: " + gamer);

            int result;
            result = random.nextInt((MAXIMUM_COMBINATION - MINIMUM_COMBINATION) + 1) + 1;

            if (result > maxScore) {
                champions.clear();

                maxScore = result;
                champion = gamer;
                champions.put(champion, maxScore);
            } else if (result == maxScore) {
                champion = gamer;
                champions.put(champion, maxScore);
            }

            System.out.println("Player " + gamer + " score: " + result);
        }

        Collection<Integer> scores = champions.values();
        String winners = scores.size() > 1 ? "Winners" : "Winner";
        System.out.println("-----------------");
        System.out.println();
        System.out.println("-----------------");
        System.out.println(winners + " of the game: ");

        for (Map.Entry<String, Integer> entry : champions.entrySet()) {
            String champ = entry.getKey();

            int score;
            score = entry.getValue();
            System.out.println(champ + " with the result " + score);
        }

        repeatDice();

    }

    // Repeat game
    private void repeatDice() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("\n");
        System.out.println("Do you want to repeat the throw? If yes, click on Latin \"y\" ");
        String input = scanner.nextLine();

        if (Objects.equals(input, "y")) {
            dice();
        }

    }

}
